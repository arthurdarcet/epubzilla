0.1.0, 2013-8-2 -- Initial release.

0.1.1, 2013-8-11
----------------

- when navigating the datastructure, if a tag element
  has child elements, return a list of those child elements
  if the name provided matches; otherwise, return an empty list

  for example:

  epub.manifest.item (returns a list of child item elments)
                ^^^^
  
  epub.manifest.itemref (returns an empty list)
