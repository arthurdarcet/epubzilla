Epubzilla  Documentation
========================

Epubzilla is a Python library for extracting data from 
EPUB documents. 

Visit the `Project Page <http://odeegan.com/epubzilla>`_ 
Check out the `Docs <http://epubzilla.odeegan.com>`_


Getting Help
----------------
If you have questions about Epubzilla, send an email to 
odeegan @ gmail . com
